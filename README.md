# Hardware.ICON-02.00

## Interfaz de Consola

Autor: 

- Agustín González

Revisión: 

- German Vazquez

Hardware:

- Conexión a todos los Botones de la Consola con su respectivo antirebote.
- Conexión a las Luces Indicadoras.
- Conexión a Buzz

Software:

- Eagle 6.5.0 Light

Usos:

- Consola